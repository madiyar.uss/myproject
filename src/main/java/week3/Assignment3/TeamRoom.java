package week3.Assignment3;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Scanner;

public class TeamRoom {
    public int id;
    public static int id_gen = 1;
    private String roomName;
    private Scanner scanner;
    private ArrayList<String> roomNames;

    public void fillRoom() throws FileNotFoundException {
        File file = new File("C:\\Users\\Madiyar\\IdeaProjects\\myproject\\src\\main\\java\\week3\\Assignment3\\roomNames.txt");
        Scanner scanner = new Scanner(file);
        while (scanner.hasNextLine()) { ;
            roomNames.add(roomName);
        }
    }

    public TeamRoom(){

    }

    private void generateId() {
        id = id_gen++;
    }

    public TeamRoom(String roomName) {
        generateId();
        setRoomName(roomName);
    }

    public String getRoomName() {
        return roomName;
    }

    public void setRoomName(String roomName) {
        this.roomName = roomName;
    }

    @Override
    public String toString() {
        return "Rooms are " + roomName ;
    }
}
