package week3.Assignment3;

public class User {
    // id (you need to generate this id by static member variable)
    // name, surname
    // username
    // password
    private int id;
    private static int id_gen = 1;
    private String name;
    private String surname;
    private String username;
    private Password password;

    private void generateId() {
        id = id_gen++;
    }

    public User(String name, String surname, String username, Password password) {
        setName(name);
        setSurname(surname);
        setUsername(username);
        setPassword(password);
    }

    public User(int id, String name, String surname, String username, Password password) {
        generateId();
        setName(name);
        setSurname(surname);
        setUsername(username);
        setPassword(password);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Password getPassword() {
        return password;
    }

    public void setPassword(Password password) {
        this.password = password;
    }

    @Override
    public String toString()
    {
        return id + " " + name + " " + surname + " " + username + " " + password.getPasswordStr();
    }
}
