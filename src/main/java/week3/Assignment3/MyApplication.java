package week3.Assignment3;
import java.io.*;
import java.sql.SQLOutput;
import java.util.ArrayList;
import java.util.Scanner;

public class MyApplication {
    private Scanner sc = new Scanner(System.in);
    private User signedUser;
    private ArrayList<User> users;

    public MyApplication() throws FileNotFoundException {
        users = new ArrayList<User>();
        fillUsers();
    }

    private void fillUsers() throws FileNotFoundException {
        File file = new File("C:\\Users\\Madiyar\\IdeaProjects\\myproject\\src\\main\\java\\week3\\Assignment3\\users.txt");
        Scanner fsc = new Scanner(file);
        while (fsc.hasNextLine()) {
            User user = new User(fsc.nextInt(), fsc.next(), fsc.next(), fsc.next(), new Password(fsc.next()));
            addUser(user);
        }
    }

    private void addUser(User user) {
        users.add(user);
    }

    private void menu() throws FileNotFoundException {
        while (true) {
            if (signedUser == null) {
                System.out.println("You are not signed in.");
                System.out.println("1. Authentication");
                System.out.println("2. Exit");
                int choice = sc.nextInt();
                if (choice == 1) authentication();
                else break;
            } else {
                userProfile();
            }
        }
    }

    private void userProfile() throws FileNotFoundException {
        while (true) {
            System.out.println("You are already signed in.");
            System.out.println("1. Sign out");
            System.out.println("2. Go to the TeamRoom");
            System.out.println("3. Exit");
            int choice = sc.nextInt();
            if (choice == 1) logOff();
            if (choice == 2) goTeamRoom();
            else break;
        }
    }

    private void goTeamRoom(){
        /*while(true) {*/
        /*TeamRoom room = new TeamRoom();
        room.fillRoom();
        System.out.printf(room); Here suppose to be implemented function to fill and show all rooms
        *//*
            System.out.println("Go to group");
            System.out.println("Go back");
            int choice = sc.nextInt();
            if (choice == 1) goTeamGroup();
            if (choice == 2) break;
        }*/
    }

    private void goTeamGroup(){
        TeamGroup group = new TeamGroup();
        System.out.println(group);
    }


    private void logOff() throws FileNotFoundException {
        signedUser = null;
        start();
    }

    private void authentication() throws FileNotFoundException {
        // sign in
        // sign up
        while (true) {
            if (signedUser != null) break;
            System.out.println("Choose your option:");
            System.out.println("1. Sign in");
            System.out.println("2. Sign up");
            System.out.println("3. Quit");
            int option = sc.nextInt();
            if (option == 1) signIn();
            else if (option == 2) signUp();
            else break;
        }
    }

    private void signIn() throws FileNotFoundException {
        Scanner scanner = new Scanner(System.in);
        String username = scanner.next();
        Password password = new Password(scanner.next());
        for (int i = 0; i < users.size(); i++) {
            User user = users.get(i);
            if (username == (user.getUsername()) && password == (user.getPassword())) {
                this.signedUser = user;
                System.out.println("Welcome!");
                userProfile();
            } else {
                System.out.println("Wrong login or password");
                break;
            }
        }
    }

    private void signUp() {
        System.out.println("Enter your name");
        String name = sc.next();
        System.out.println("Enter surname");
        String surname = sc.next();
        System.out.println("Enter username");
        String username = sc.next();
        System.out.println("Enter password");
        User newUser = new User(name, surname, username, new Password(sc.next()));
        addUser(newUser);
    }

    public void start() throws FileNotFoundException {
        while (true) {
            System.out.println("Welcome to my application!");
            System.out.println("Select command:");
            System.out.println("1. Menu");
            System.out.println("2. Exit");
            int choice = sc.nextInt();
            if (choice == 1) {
                menu();
            } else {
                break;
            }
        }

        // save the userlist to db.txt
    }

    private void saveUserList() throws IOException {

    }

}

