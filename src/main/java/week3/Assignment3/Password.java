package week3.Assignment3;

public class Password {
    private String password;

    public Password(String password) {
        setPasswordStr(password);
    }

    public String getPasswordStr() {
        return password;
    }

    public void setPasswordStr(String password) {
        if (checkFormat(password))
            this.password = password;
        else
            System.out.println("Incorrect password format! Try again.");
    }

    public static boolean checkFormat (String password) {
        int pass_length = password.length();
        if (pass_length >= 9) {
            for (int i = 0; i < pass_length; i++) {
                char pass_char = password.charAt(i);
                if ((pass_char >= 'A' || pass_char <= 'Z') && (pass_char >= 'a' || pass_char <= 'z') && (pass_char >= '0' || pass_char <= '9')) {
                    return true;
                }
                return false;
        }
        }
        return false; }
}
