package week3.Assignment3;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws IOException, FileNotFoundException {
        MyApplication application = new MyApplication();
        System.out.println("An application is about to start...");
        application.start();
    }
}
