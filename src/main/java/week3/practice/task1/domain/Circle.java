package week3.practice.task1.domain;

public class Circle extends Shape {
    private double radius;

    public Circle() {

        radius = 1.0;
    }

    public Circle(Color color, boolean filled, double radius) {
        super(color, filled);
        setRadius(radius);
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    public double getArea() {
        double area = Math.PI * Math.pow(radius, 2);
        return area; //dada
    }

    public double getPerimeter() {
        double circumference = 2 * Math.PI * radius;
        return circumference;//dadadadadaw
    }

    @Override
    public String toString() {
        return "Shape Circle " + super.toString() + " " + radius;
    }
}
