package week3.practice.task1.domain;

public class Square extends Rectangle {
    private double side;

    public Square(){

    }

    public Square(double side){
        super(side,side);
    }

    public Square(Color color, boolean filled, double side){
        super(color, filled, side, side);
    }

    public double getSide() {
        return side;
    }

    public void setSide(double side) {
        super.setLength(side);
        super.setWidth(side);
    }

    @Override

    public void setWidth(double side){
       setSide(side);

    }

    @Override

    public void setLength(double side){
        setSide(side);
    }

    @Override
    public String toString() {
        return "Shape is Square " + super.toString();
    }
}
