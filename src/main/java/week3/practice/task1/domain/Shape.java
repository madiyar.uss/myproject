    package week3.practice.task1.domain;

public class Shape {
    private Color color;
    private boolean filled;

    public Shape(){
        color = Color.Green;
        filled =  true;
    }

    public Shape(Color color, boolean filled){
        setColor(color);
        setFilled(filled);
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public boolean isFilled() {
        return filled;
    }

    public void setFilled(boolean filled) {
        this.filled = filled;
    }

    @Override
    public String toString() {
        return "Color is " + color + " Filled " + filled;
    }
}
