package week3.practice.task1.domain;

public class Rectangle extends Shape {
    private double width;
    private double length;

    public Rectangle(){

    }

    public Rectangle(double width, double length){
        setWidth(width);
        setLength(length);
    }

    public Rectangle(Color color, boolean filled, double width, double length){
        super(color, filled);
        setWidth(width);
        setLength(length);
    }

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public double getLength() {
        return length;
    }

    public void setLength(double length) {
        this.length = length;
    }

    public double getArea(){
        double area = length * width;
        return area;
    }

    public double getPerimeter(){
        double P = (length + width) * 2;
        return P;
    }

    @Override
    public  String toString() {
        return "Shape Rectangle " + super.toString() + " " + width + " " + length;
    }
}
